﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Threading;

namespace AnuTronc
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Boolean is_param = false;
        Boolean is_started = false;

        static SerialPort _serialPort;  

        int input_to_param;

        Key P1_UP;
        Key P1_DOWN;
        Key P1_LEFT;
        Key P1_RIGHT;

        Key P2_UP;
        Key P2_DOWN;
        Key P2_LEFT;
        Key P2_RIGHT;




        public MainWindow()
        {
            InitializeComponent();

        }

        private void get_input(object sender, KeyEventArgs e)
        {
            if (is_param)
            {
                switch (input_to_param)
                {
                    case 1: P1_UP = e.Key; button_P1_UP.Content = P1_UP; break;
                    case 2: P1_DOWN = e.Key; button_P1_DOWN.Content = P1_DOWN; break;
                    case 3: P1_LEFT = e.Key; button_P1_LEFT.Content = P1_LEFT; break;
                    case 4: P1_RIGHT = e.Key; button_P1_RIGHT.Content = P1_RIGHT; break;
                    case 5: P2_UP = e.Key; button_P2_UP.Content = P2_UP; break;
                    case 6: P2_DOWN = e.Key; button_P2_DOWN.Content = P2_DOWN; break;
                    case 7: P2_LEFT = e.Key; button_P2_LEFT.Content = P2_LEFT; break;
                    case 8: P2_RIGHT = e.Key; button_P2_RIGHT.Content = P2_RIGHT; break;
                    default: break;
                }
                is_param = false;

            }
            else
            {
                if (is_started)
                {
                    if (e.Key == P1_UP)
                    {
                        Console.WriteLine("P1UP");
                        _serialPort.Write("P1UP");
                    }
                    else if (e.Key == P1_DOWN)
                    {
                        Console.WriteLine("P1DOWN");
                        _serialPort.Write("P1DOWN");
                    }
                    else if (e.Key == P1_LEFT)
                    {
                        Console.WriteLine("P1LEFT");
                        _serialPort.Write("P1LEFT");
                    }
                    else if (e.Key == P1_RIGHT)
                    {
                        Console.WriteLine("P1RIGHT");
                        _serialPort.Write("P1RIGHT");
                    }
                    else if (e.Key == P2_UP)
                    {
                        Console.WriteLine("P2UP");
                        _serialPort.Write("P2UP");
                    }
                    else if (e.Key == P2_DOWN)
                    {
                        Console.WriteLine("P2DOWN");
                        _serialPort.Write("P2DOWN");
                    }
                    else if (e.Key == P2_LEFT)
                    {
                        Console.WriteLine("P2LEFT");
                        _serialPort.Write("P2LEFT");
                    }
                    else if (e.Key == P2_RIGHT)
                    {
                        Console.WriteLine("P2RIGHT");
                        _serialPort.Write("P2RIGHT");
                    }
                }
            }
        }

        private void Button_P1_RIGHT_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 4;
            is_param = true;

        }

        private void Button_P1_LEFT_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 3;
            is_param = true;
        }

        private void Button_P1_DOWN_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 2;
            is_param = true;
        }

        private void Button_P1_UP_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 1;
            is_param = true;
        }

        private void Button_P2_UP_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 5;
            is_param = true;
        }

        private void Button_P2_DOWN_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 6;
            is_param = true;
        }

        private void Button_P2_LEFT_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 7;
            is_param = true;
        }

        private void Button_P2_RIGHT_Click(object sender, RoutedEventArgs e)
        {
            input_to_param = 8;
            is_param = true;
        }

        private void Button_start_Click(object sender, RoutedEventArgs e)
        {
            is_started = true;
            Console.WriteLine("STARTGAME");
            _serialPort.Write("STARTGAME    ");
        }

        private void Connect_button_Click(object sender, RoutedEventArgs e)
        {
            _serialPort = new SerialPort();
            _serialPort.PortName = port_box.Text;//Set your board COM
            _serialPort.BaudRate = 9600;
            _serialPort.Open();
        }
    }
}
