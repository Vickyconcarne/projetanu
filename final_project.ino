//////////////////////
// Library Includes //
//////////////////////
#include <stdlib.h>
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

// R0, G0, B0, R1, G1, B1 should be connected to pins
// 2, 3, 4, 5, 6, and 7 respectively. Their pins aren't defined,
// because they're controlled directly in the library. These pins
// can be moved (somewhat):

#define CLK 11
#define LAT 10
#define OE  9

#define A   A0
#define B   A1
#define C   A2
#define D   A4

#define GREEN 1920
#define RED 61440
#define YELLOW 63360
#define BLUE 0x001F
#define WHITE 0xFFFF 

RGBmatrixPanel matrix(A, B, C, D, CLK, LAT, OE, false);

enum directions {
  UP,    // 0
  DOWN,  // 1
  LEFT,  // 2
  RIGHT  // 3
};

enum player_type {
  PLAYER1,
  PLAYER2,
  NO_ONE
};

enum scene {
  MENU,
  GAME,
  VICTORY
};

// définition du player 1, couleur jaune
int player1_x = 0;  
int player1_y = 0;
directions direction1 = RIGHT ;

// définition du player 2, couleur rouge
int player2_x = 31;  
int player2_y = 31;
directions direction2 = LEFT ;

// définition du perdant (PLAYER1, PLAYER2 ou NO_ONE quand la partie continue), lorsque l'on touche une couleur
player_type looser = NO_ONE;
// définition de la logique de niveau, les curseurs ne peuvent pas etre appellés si on est encore dans le menu
//Et on a pas besoin de reeffacer tout ou tout redessiner dans le menu si on a deja initialisé
scene in_what_scene = MENU;
bool initialising_scene = true;

void setup() {
  matrix.begin();       // Initialize the matrix.
  Serial.begin(9600);   // Start serial
  randomSeed(analogRead(8));
}

String input  ;
void loop() {
  
  if (Serial.available() > 0) {
    input = String(Serial.readString());
  }

  //////////////////////
  // Si on est en jeu //
  //////////////////////
  if (in_what_scene == GAME){ 
    if (initialising_scene == true){ //Si on a pas encore initialisé la scène de jeu
      initialise_game();
      initialising_scene = false;
    }
    else{ //Sinon on continue à faire les checks dans le jeu (déplacement et condition de victoire)
      changeDirection(input);   
      delay(500);
      if (!moveCursor(direction1, &player1_x, &player1_y)){
        //P2 wins, passage à la scene victoire
        looser = PLAYER1;
        in_what_scene = VICTORY;
        initialising_scene = true;
      }
      if (!moveCursor(direction2, &player2_x, &player2_y)){
        //P1 wins, passage à la scene victoire
        looser = PLAYER2;
        in_what_scene = VICTORY; 
        initialising_scene = true;
      }
    }
  }

  ///////////////////////
  // Si on est en menu //
  //////////////////////
  else if (in_what_scene == MENU){ 
    
    if (initialising_scene == true){ //Si on a pas encore initialisé la scène de menu
      initialise_menu();
      initialising_scene = false;
    }
    else{ //Sinon on continue à faire les checks dans le menu (appui pour commencer la partie)
      if (input.startsWith("STARTGAME")){
        in_what_scene = GAME; 
        initialising_scene = true;
      }
    }  
  }
  
  ////////////////////////////////////
  // Si on est en scene de victoire //
  ///////////////////////////////////
  else{ 
    if (initialising_scene == true){ //Si on a pas encore initialisé la scène de menu
      initialise_victory();
      initialising_scene = false;
      delay(4000);
      in_what_scene = MENU; //On repasse au menu après 4 secondes
      initialising_scene = true;
    }
  }
  
}

void initialise_game(){
  matrix.fillScreen(0);
  matrix.drawPixel(player1_x,player1_y,YELLOW);   
  matrix.drawPixel(player2_x,player2_y,RED);
}

void initialise_menu(){
  matrix.fillScreen(0);
  looser = NO_ONE;
  // TRON
  matrix.drawChar(8,5,'t',GREEN,0,1);
  matrix.drawChar(13,5,'r',GREEN,0,1);
  matrix.drawChar(18,5,'o',GREEN,0,1);
  matrix.drawChar(23,5,'n',GREEN,0,1);
  // PRESS
  matrix.drawChar(7,14,'p',YELLOW,0,1);
  matrix.drawChar(13,14,'r',YELLOW,0,1);
  matrix.drawChar(18,14,'e',YELLOW,0,1);
  matrix.drawChar(23,14,'s',YELLOW,0,1);
  matrix.drawChar(27,14,'s',YELLOW,0,1);
  // START
  matrix.drawChar(5,23,'s',YELLOW,0,1);
  matrix.drawChar(11,23,'t',YELLOW,0,1);
  matrix.drawChar(16,23,'a',YELLOW,0,1);
  matrix.drawChar(21,23,'r',YELLOW,0,1);
  matrix.drawChar(26,23,'t',YELLOW,0,1);
}

void initialise_victory(){
  matrix.fillScreen(0);
  // WIN
  matrix.drawChar(8,5,'w',GREEN,0,1);
  matrix.drawChar(13,5,'i',GREEN,0,1);
  matrix.drawChar(18,5,'n',GREEN,0,1);
  // PLAYER
  if (looser == PLAYER1){
    matrix.drawChar(1,14,'p',RED,0,1);
    matrix.drawChar(6,14,'l',RED,0,1);
    matrix.drawChar(11,14,'a',RED,0,1);
    matrix.drawChar(16,14,'y',RED,0,1);
    matrix.drawChar(21,14,'e',RED,0,1);
    matrix.drawChar(26,14,'r',RED,0,1);
  }
  else{
    matrix.drawChar(1,14,'p',YELLOW,0,1);
    matrix.drawChar(6,14,'l',YELLOW,0,1);
    matrix.drawChar(11,14,'a',YELLOW,0,1);
    matrix.drawChar(16,14,'y',YELLOW,0,1);
    matrix.drawChar(21,14,'e',YELLOW,0,1);
    matrix.drawChar(26,14,'r',YELLOW,0,1);
  }
  // 1 or 2
  if (looser == PLAYER1){
    matrix.drawChar(5,23,'t',RED,0,1);
    matrix.drawChar(11,23,'w',RED,0,1);
    matrix.drawChar(16,23,'o',RED,0,1);
  }
  else{
    matrix.drawChar(5,23,'o',YELLOW,0,1);
    matrix.drawChar(11,23,'n',YELLOW,0,1);
    matrix.drawChar(16,23,'e',YELLOW,0,1);
  }
}


void changeDirection(String commande){
  if (commande.startsWith("P1UP")){
      direction1 = UP;
    }
    else if (commande.startsWith("P1RIGHT")){
      direction1 = RIGHT;
    }
    else if (commande.startsWith("P1DOWN")){
      direction1 = DOWN;
    }
    else if (commande.startsWith("P1LEFT")){
      direction1 = LEFT;
    }
    else if (commande.startsWith("P2UP")){
      direction2 = UP;
    }
    else if (commande.startsWith("P2RIGHT")){
      direction2 = RIGHT;
    }
    else if (commande.startsWith("P2DOWN")){
      direction2 = DOWN;
    }
    else if (commande.startsWith("P2LEFT")){
      direction2 = LEFT;
    }
}

//déplace l'abscisse et l'ordonnée selon la direction dir et renvoie true si la case est libre false sinon
bool moveCursor(directions dir,int* x,int* y)
{
  switch (dir)
  {
    case UP:
      if (*y == 0) { *y = 31; }
      else { *y = *y-1; }
      break;
    case DOWN:
      if (*y == 31) { *y = 0; }
      else { *y = *y+1; }
      break;
    case LEFT:
      if (*x == 0) { *x = 31; }
      else { *x = *x-1;}
      break;
    case RIGHT:
      if (*x == 31) { *x = 0; }
      else { *x = *x+1; }
      break;
    default:
    break;
  }
  return (getPixelColor(*x,*y) == 0);
}


uint16_t getPixelColor(uint8_t x, uint8_t y)
{
  uint8_t * ptr; // A movable pointer to read data from
  uint16_t r = 0, g = 0, b = 0; // We'll store rgb values in these
  uint8_t nPlanes = 4; // 4 planes. This is static in the library
  uint8_t nRows = matrix.height() / 2; // 16 for 32 row, 8 for 16

  // Get a pointer to the matrix backBuffer, where the pixel/
  // color data is stored:
  uint8_t * buf = matrix.backBuffer();

  // Data for upper half is stored in lower bits of each byte
  if (y < nRows)
  {
    ptr = &buf[y * matrix.width() * (nPlanes - 1) + x];
    // get data from plane 0:
    if (ptr[64] & 0x01)
      r = 1;
    if (ptr[64] & 0x02)
      g = 1;
    if (ptr[32] & 0x01)
      b = 1;
    // get data from other planes:
    for (uint8_t i = 2; i < (1 << nPlanes);  i <<= 1)
    {
      if (*ptr & 0x04)
        r |= i;
      if (*ptr & 0x08)
        g |= i;
      if (*ptr & 0x10)
        b |= i;
      ptr += matrix.width();
    }
  }
  else
  {
    ptr = &buf[(y - nRows) * matrix.width() * (nPlanes - 1) + x];
    // get data from plane 0
    if (ptr[32] & 0x02)
      r = 1;
    if (*ptr & 0x01)
      g = 1;
    if (*ptr & 0x02)
      b = 1;
    for (uint8_t i = 2; i < (1 << nPlanes); i <<= 1)
    {
      if (*ptr & 0x20)
        r |= i;
      if (*ptr & 0x40)
        g |= i;
      if (*ptr & 0x80)
        b |= i;
      ptr += matrix.width();
    }
  }
  return (r << 12) | (g << 7) | (b << 1);
}
